<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>A01_VIGIL</title>
<style type="text/css">
	body {
		font-family: "Helvetica Nue",helvetica,arial,sans-serif;
		font:14px;
		font-color:#333333;
		margin: auto;
		max-width: 900px;
		background-image: url("../conf/images/stucco/stucco.png");
		background-repeat: repeat;
	}
	header{
		width: 900px;
		background-color: black;
		text-align: right;
	}
	footer {
		width: 900px;
		background-color: black;
		text-align: center;
		color: gray;
		
	}
	header ul{
		display: inline; 
	}
	header ul a{
		text-decoration: none;
		color:gray;
	}
	.headMenu{
		text-decoration: none;
	}
	
 ul {
    display: inline;
    text-align: left;
}
	p strong{
		color:green;
	}
	.myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #e9e9e9));
	background:-moz-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:-webkit-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:-o-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:-ms-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
	background:linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#e9e9e9',GradientType=0);
	background-color:#f9f9f9;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color:#666666;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e9e9e9), color-stop(1, #f9f9f9));
	background:-moz-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:-webkit-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:-o-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:-ms-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
	background:linear-gradient(to bottom, #e9e9e9 5%, #f9f9f9 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e9e9e9', endColorstr='#f9f9f9',GradientType=0);
	background-color:#e9e9e9;
}
.myButton:active {
	position:relative;
	top:1px;
}
	.simlar{
		width: 180px;
border: solid 1pt #e7e7e7;
padding: 0.5em;
margin-right: 1em;
float: left;
	}
</style>
</head>

<body>
	<header>
		<ul><a href="#">My Account</a></ul>
	  <ul><a href="#">Wish List</a></ul>
		<ul><a href="#">Shopping Cart</a></ul>
		<ul><a href="#">Checkout</a></ul>

</header>
<h1>Art Store</h1>
    <div class="headMenu">
      <ul>
		  <a href="#">Home</a></ul>
		<ul><a href="about.php">About Us</a></ul>
		<ul><a href="#">Art Works</a></ul>
		<ul><a href="#">Artists</a></ul>
    </div>
<h2>Self-portrait in a Straw Hat</h2>
<p>By <a href="#">Louise Elisabeth Lebrun</a></p>
<div><p style="float:left;"><img src="../conf/images/113010.jpg" width=300px float:left margin-top:1em alt="self-portrait in a Straw Hat"/></p>
<p>The Painting appeals, after cleaning, to be an autograph replica of a picture, the original of which was a painted in Brussels in 1792 in free imitation of Rubens's 'Chapeau de Paile', which LeBrun had seen in Antwerp. It was exhibited in Paris in 1782 at the Salon de la correspondance.</p>
<p><strong>$700</strong></p>
	<button class="myButton">
	<p><a href="#" class="myButton">Add to wish List</a>
	</button><button>
	  <a href="#" class="myButton">Add to Shopping Cart</a>
	  </button>
  </p>
	<h3><strong>Project Details</strong></h3>
	<table width="300" >
  <tbody>
    <tr>
      <td><strong>Date:</strong></td>
      <td>1782</td>
    </tr>
    <tr>
      <td><strong>Medium:</strong></td>
      <td>oil on canvas</td>
    </tr>
    <tr>
      <td><strong>Dimensions:</strong></td>
      <td>98cm x 71cm</td>
    </tr>
    <tr>
      <td><strong>Home:</strong></td>
      <td><a href="https://www.nationalgallery.org.uk/">National Gallery, London</a></td>
    </tr>
    <tr>
      <td><strong>Genres:</strong></td>
      <td><a href="https://en.wikipedia.org/wiki/Realism_(arts)">Realism, Rococo</a></td>
    </tr>
    <tr>
      <td><strong>Subject:</strong></td>
      <td><a href="https://imgur.com/gallery/jWr67J8">People, Art</a></td>
    </tr>
  </tbody>
</table>

	</div>
	<h3><strong>Similar Products</strong></h3>
<p><img class="simlar"src="../conf/images/thumbs/116010.jpg" width="132" height="132" alt="Artist Holding a Thisitle"><img class="simlar"src="../conf/images/thumbs/120010.jpg" width="132" height="132" alt="Portait of Eleanor of Toledo"/><img class="simlar"src="../conf/images/thumbs/107010.jpg" width="132" height="132" alt="Madame de Pompadour"/><img class="simlar"src="../conf/images/thumbs/106020.jpg" width="130" height="132" alt="Girl with a Pearl Earing"/></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
	<p ><a href="https://www.louvre.fr/en/oeuvre-notices/self-portrait-or-portrait-artist-holding-thistle">Artist Holding a Thisitle</a> &emsp; &emsp; &emsp; <a href="https://www.khanacademy.org/humanities/renaissance-reformation/high-ren-florence-rome/pontormo/a/bronzino-portrait-of-eleonora-of-toledo-with-her-son-giovanni">Portait of Eleanor of Toledo</a>&emsp;&emsp; &emsp;<a href="https://www.smithsonianmag.com/smart-news/madame-de-pompadour-was-far-more-mistress-180967662/">Madame de Pompadour</a> &emsp; &emsp; &emsp;<a href="https://www.britannica.com/topic/Girl-with-a-Pearl-Earring-by-Vermeer">Girl with a Pearl Earing</a></p>
	<p>&nbsp;</p>
	<footer>All images are copyright to Their owners. This is justa hypothetical site.</footer>
</body>
</html>